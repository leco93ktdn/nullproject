package com.adcvn.adctransport.action;

import android.os.Bundle;
import com.adcvn.adctransport.R;

public class MainActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
