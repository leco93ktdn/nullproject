package com.adcvn.adctransport.common;

import android.content.Context;
import android.content.SharedPreferences;

public class Cache {

    // lưu cache dữ liệu vào thiết bị
    public static void savePreferenceSession(Context context, String prefName,
                                          String key, String value) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                prefName, 0);
        sharedPreferences.edit().putString(key, value).commit();
    }

    // lấy dữ liệu cache từ thiết bị
    public static String loadPreferenceSession(Context context, String prefName,
                                            String key) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                prefName, 0);
        return sharedPreferences.getString(key, "");
    }

    // kiểm tra key tồn tại trong cache của thiết bị
    public static boolean hasPreferenceSession(Context context, String prefName,
                                        String key) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                prefName, 0);
        return sharedPreferences.contains(key);
    }

    // xóa key tồn tại trong cache của thiết bị
    public static void removePreferenceSession(Context context, String prefName, String key) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                prefName, 0);
        sharedPreferences.edit().remove(key).commit();
    }

}
