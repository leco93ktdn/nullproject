package com.adcvn.adctransport.common;

import org.threeten.bp.temporal.ChronoUnit;

import java.time.Duration;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.StringTokenizer;

public class SystemDateTime {

    // get current time (2019-11-20)
    public static String getDateTimeCurrent(){
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int monthOfYear = calendar.get(Calendar.MONTH)+1;
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        String day =  String.valueOf(dayOfMonth).length() == 1 ? 0+String.valueOf(dayOfMonth): String.valueOf(dayOfMonth);
        String month =  String.valueOf(monthOfYear).length() == 1 ? 0+String.valueOf(monthOfYear): String.valueOf(monthOfYear);
        String date = year+ "-" + month
                + "-"+ day;
        return date;
    }
  public static int getBetweenTwoDate(String oldDate){
      int betweenDate = 0;
      if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
          LocalDate d1 = LocalDate.parse(oldDate, DateTimeFormatter.ISO_LOCAL_DATE);
          LocalDate d2 = LocalDate.parse(getDateTimeCurrent(), DateTimeFormatter.ISO_LOCAL_DATE);
          Duration diff = Duration.between(d1.atStartOfDay(), d2.atStartOfDay());
          betweenDate = (int) diff.toDays();
      }else{
          org.threeten.bp.LocalDate d1 = org.threeten.bp.LocalDate.parse(oldDate, org.threeten.bp.format.DateTimeFormatter.ISO_DATE_TIME);
          org.threeten.bp.LocalDate d2 = org.threeten.bp.LocalDate.parse(getDateTimeCurrent(), org.threeten.bp.format.DateTimeFormatter.ISO_DATE_TIME);
          betweenDate = (int) ChronoUnit.DAYS.between(d1, d2);
      }
      return betweenDate;
  }

    public static String formatDateToClient(String date){
        String resultDate = "";
        StringTokenizer st = new StringTokenizer(date, "-");
        List<String> dateElements = new ArrayList<String>();
        while (st.hasMoreTokens()) {
            dateElements.add(st.nextToken());
        }
        for(int i = dateElements.size()-1; i>=0;i--){
            resultDate += dateElements.get(i)+"/";
        }
        resultDate = resultDate.substring(0, resultDate.length() - 1);

        return resultDate;
    }

    public static String formatDateToServer(String date){
        String resultDate = "";
        StringTokenizer st = new StringTokenizer(date, "/");
        List<String> dateElements = new ArrayList<String>();
        while (st.hasMoreTokens()) {
            dateElements.add(st.nextToken());
        }
        for(int i = dateElements.size()-1; i>=0;i--){
            resultDate += dateElements.get(i)+"-";
        }
        resultDate = resultDate.substring(0, resultDate.length() - 1);

        return resultDate;
    }

}
